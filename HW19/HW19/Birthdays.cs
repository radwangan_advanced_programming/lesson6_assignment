﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HW19
{
    public partial class Birthdays : Form
    {
        private string _userName;
        private string _dateToAdd;

        public Birthdays()
        {
            InitializeComponent();
        }
        public string Username
        {
            get { return _userName; }
            set { _userName = value;}
        }
        private void Birthdays_Load(object sender, EventArgs e)
        {

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            // here i need to write the code
            string filePath = @"C:\Users\kfir\Desktop\user\" + this._userName+"BD.txt";
            System.IO.StreamReader file = new System.IO.StreamReader(filePath);
            string data;
            string[] arr = new string[2];
            bool isBirthday = false;
            string date1 = e.Start.ToShortDateString();
            string[] date2 = new string[3];

            while ((data = file.ReadLine()) != null)
            {
                arr = data.Split(',');
                if (Convert.ToDateTime(arr[1]) == Convert.ToDateTime(date1))
                {
                    isBirthday = true;
                    break;
                }
            }
            if (isBirthday)
            {
                this.label2.Text = "בתאריך הנבחר – "+ arr[0] +" חוגג יום הולדת!";
            }
            else
            {
                this.label2.Text = "בתאריך הנבחר – אף אחד לא חוגג יום הולדת !!";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (_dateToAdd != null && this.textBox1.Text != null)
            {
                string filePath = @"C:\Users\magshimim\Desktop\HW19\HW19\" + this._userName + "BD.txt";
                string data = System.IO.File.ReadAllText(filePath);
                data = data + "\r\n" + this.textBox1.Text + "," + this._dateToAdd;
                System.IO.File.WriteAllText(filePath, data);
            }
            else
            {
                MessageBox.Show("Error, the new name and date is not added!!! please check the name or reselect the date!");
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            string[] arr;
            this._dateToAdd = this.dateTimePicker1.ToString();
            arr = this._dateToAdd.Split(' ');
            this._dateToAdd = arr[2];
            // here i need to continue to update the date
            arr = this._dateToAdd.Split('/');
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i].Length==2 && arr[i][0] == '0')
                {
                    arr[i] = ""+arr[i][1];
                }
            }
            this._dateToAdd = arr[1] + "/" + arr[0] + "/" + arr[2];
        }
    }
}
